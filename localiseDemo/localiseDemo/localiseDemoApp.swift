//
//  localiseDemoApp.swift
//  localiseDemo
//
//  Created by Thukaram Kethavath on 8/24/23.
//

import SwiftUI

@main
struct localiseDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
